﻿#include <cmath>
#include <iostream>

class Vector
{

private:

    double x;
    double y;
    double z;
    double l;

public:

    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        std::cout << x << ' ' << y << ' ' << z << '\n';
    }

    void Lenght()
    {
        l = sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
        std::cout << l << '\n';
    }
};

int main()
{
    Vector v(5, 5, 5);
    v.Show();
    v.Lenght();
}